﻿[System.Serializable]
public class Database
{
    #region Public Variables
    public int[] PadsInCounter = new int[4];
    public int[] PadsOutCounter = new int[4];
    public float TimeInGame;
    public float[] TimeOnPad = new float[4];
    #endregion

    #region Public Methods
    public Database(AnimationHandler variables)
    {
        PadsInCounter = variables.PadsInCounter;
        PadsOutCounter = variables.PadsOutCounter;
        TimeInGame = variables.TimeInGame;
        TimeOnPad = variables.TimeOnPad;
    }
    #endregion
}