﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventsHandler : MonoBehaviour
{
    #region Private Variables
    private GameObject[] _objects;
    #endregion

    #region Private Methods
    private void Start()
    {
        _objects = new GameObject[this.transform.childCount];

        for (int i = 0; i < _objects.Length; i++)
        {
            _objects[i] = this.gameObject.transform.GetChild(i).gameObject;
        }
    }

    private void ChangeColor()
    {
        if (_objects[0].GetComponent<Renderer>().material.color == Color.blue)
            _objects[0].GetComponent<Renderer>()?.material.SetColor("_Color", Color.red);
        else
            _objects[0].GetComponent<Renderer>()?.material.SetColor("_Color", Color.blue);
    }

    private void ChangeScale()
    {
        if (_objects[1].transform.localScale == new Vector3(1, 1, 1))
            _objects[1].transform.localScale = new Vector3(2, 2, 2);
        else
            _objects[1].transform.localScale = new Vector3(1, 1, 1);
    }

    private void ChangeRotation()
    {
        if (_objects[2].gameObject.transform.rotation == Change(0, 0, 0, 1))
            _objects[2].gameObject.transform.rotation = Change(0.4f, -0.1f, 0.4f, 0.9f);
        else
            _objects[2].gameObject.transform.rotation = Change(0, 0, 0, 1);
    }

    private static Quaternion Change(float x, float y, float z, float w)
    {
        Quaternion quaternion = new Quaternion();
        quaternion.Set(x, y, z, w);
        return quaternion;
    }
    #endregion
}
