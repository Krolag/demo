﻿using System.Collections;
using UnityEngine;

public class AnimationHandler : MonoBehaviour
{
    #region Private Values
    [SerializeField] private Animator[] _animators;
    private bool _isCourtineExecuting = false;
    #endregion

    #region Public Values
    public int[] PadsInCounter = new int[4];
    public int[] PadsOutCounter = new int[4];
    public float[] TimeOnPad = new float[4];
    public float TimeInGame = 0;
    #endregion

    #region Private Methods
    private void Awake()
    {
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Anima");
        PadsInCounter = new int[objects.Length];
        PadsOutCounter = new int[objects.Length];
        TimeOnPad = new float[objects.Length];

        Database data = SaveDatabase.LoadPlayer();
        if (data != null)
        {
            PadsInCounter = data.PadsInCounter;
            PadsOutCounter = data.PadsOutCounter;
            TimeInGame = data.TimeInGame;
            TimeOnPad = data.TimeOnPad;
        }
    }

    private void Start()
    {
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Anima");
        _animators = new Animator[objects.Length];
        
        for (int i = 0; i < objects.Length; i++)
            _animators[i] = objects[i].GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "BlueAcivator")
        {
            TurnOnAnimation(_animators[0]);
            PadsInCounter[0]++;
        }
        if (other.gameObject.name == "GreenAcivator")
        {
            TurnOnAnimation(_animators[1]);
            PadsInCounter[1]++;
        }
        if (other.gameObject.name == "PinkAcivator")
        {
            TurnOnAnimation(_animators[2]);
            PadsInCounter[2]++;
        }
        if (other.gameObject.name == "RedAcivator")
        {
            TurnOnAnimation(_animators[3]);
            PadsInCounter[3]++;
        }
    }

    private void Update()
    {
        TimeInGame += Time.deltaTime;
        StartCoroutine(SavingAfterTime(1));
    }

    IEnumerator SavingAfterTime(float time)
    {
        if (_isCourtineExecuting)
            yield break;

        _isCourtineExecuting = true;
        yield return new WaitForSeconds(time);
        SaveDatabase.SavePlayer(this.GetComponent<AnimationHandler>());
        _isCourtineExecuting = false;
        Debug.Log("SAVE!");
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name == "BlueAcivator") 
            TimeOnPad[0] += Time.deltaTime;
        if (other.gameObject.name == "GreenAcivator") 
            TimeOnPad[1] += Time.deltaTime;
        if (other.gameObject.name == "PinkAcivator")
            TimeOnPad[2] += Time.deltaTime;
        if (other.gameObject.name == "RedAcivator") 
            TimeOnPad[3] += Time.deltaTime;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "BlueAcivator")
        {
            TurnOffAnimation(_animators[0]);
            PadsOutCounter[0]++;
        }
        if (other.gameObject.name == "GreenAcivator")
        {
            TurnOffAnimation(_animators[1]);
            PadsOutCounter[1]++;
        }
        if (other.gameObject.name == "PinkAcivator")
        {
            TurnOffAnimation(_animators[2]);
            PadsOutCounter[2]++;
        }
        if (other.gameObject.name == "RedAcivator")
        {
            TurnOffAnimation(_animators[3]);
            PadsOutCounter[3]++;
        }
    }

    private void TurnOnAnimation(Animator animator)
    {
        animator.SetBool("ExecuteAnima", true);
    }

    private void TurnOffAnimation(Animator animator)
    {
        animator.SetBool("ExecuteAnima", false);
    }
    #endregion
}
